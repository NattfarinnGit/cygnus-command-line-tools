class Entity:
    def __init__(self, tree):
        if tree.has("vnum"):
            self.vnum = tree.get("vnum").get_value()

        if tree.has("progs"):
            self.progs = [Program(parser.Tree(prog)) for prog in tree.get(
                "progs").get_all("prog")]


class Program:
    def __init__(self, tree):
        program = tree.get_values()
        (self.type,
         self.arguments,
         self.body) = program


class Mobile(Entity):
    def __init__(self, tree):
        super().__init__(tree)

        texts = tree.get("mobile_texts").get_values()
        self.keywords = texts[0].split(" ")
        (self.name,
         self.display,
         self.description) = texts[1:]

        if tree.has("mobile_grammar_case"):
            self.grammar = tree.get("mobile_grammar_case").get_values()
        else:
            self.grammar = [self.name] * 7

        act = tree.get("mobile_act").get_values()
        (self.act,
         self.affected_by,
         self.alignment) = act[:3]

        self.is_complex = act[3] == "C"

        level_dices = tree.get("mobile_level_dices").get_values()
        (self.level,
         self.to_hit,
         self.armor_class,
         self.hit_dice,
         self.damage_dice) = level_dices

        pos_sex = tree.get("mobile_pos_sex").get_values()
        (self.position,
         self.def_position,
         self.sex) = pos_sex

        # Cygnus Reborn
        if tree.has("mobile_money_exp"):
            money_exp = tree.get("mobile_money_exp").get_values()
            (self.money,
             self.exp) = money_exp
        # Cygnus Division
        elif tree.has("mobile_exp") and tree.has("mobile_new_money"):
            self.money = tree.get("mobile_new_money").get_values()
            self.exp = tree.get("mobile_exp").get_value()

        # Complex mobile
        if self.is_complex and tree.has("mobile_complex"):
            self.stats = tree.get("mobile_stats").get_values()
            self.saves = tree.get("mobile_saves").get_values()

            race_class = tree.get("mobile_race_class").get_values()
            (self.race,
             self.class_type,
             self.height,
             self.weight,
             self.languages_known,
             self.language,
             self.num_attacks) = race_class

            corpse_rolls = tree.get("mobile_corpse_rolls").get_values()
            if corpse_rolls[0] == "E":
                self.corpse_vnum = corpse_rolls[9]
                corpse_rolls = corpse_rolls[1:9]

            (self.hit_roll,
             self.damage_roll,
             self.body_parts,
             self.resistant,
             self.immune,
             self.susceptible,
             self.attacks,
             self.defenses) = corpse_rolls


class Object(Entity):
    def __init__(self, tree):
        super().__init__(tree)

        texts = tree.get("object_texts").get_values()
        self.keywords = texts[0].split(" ")
        (self.name,
         self.display,
         self.description) = texts[1:]

        if tree.has("object_grammar_case"):
            self.grammar = tree.get("object_grammar_case").get_values()
        else:
            self.grammar = [self.name] * 7

        object_type = tree.get("object_type").get_values()
        (self.type,
         self.extra_flags,
         self.wear_flags) = object_type[:3]
        if len(object_type) > 3:
            self.layers = object_type[3]

        self.values = tree.get("object_values").get_values()

        # Cygnus Reborn
        if tree.has("object_weight_size"):
            weight_size = tree.get("object_weight_size").get_values()
            (self.weight,
             self.cost,
             self.material_type) = weight_size[:3]
            if len(weight_size) > 3:
                self.sex = weight_size[3]
            if len(weight_size) > 4:
                self.size = weight_size[4]
        # Cygnus Division
        elif tree.has("object_weight"):
            self.weight = tree.get("object_weight").get_value()
            self.cost = tree.get("object_price").get_values()
            size = tree.get("object_size").get_values()
            (self.material_type,
             self.sex) = size[:2]
            if len(size) > 2:
                self.size = size[2]

        if tree.has("object_spell_names"):
            self.spell_names = tree.get("object_spell_names").get_values()

        self.modifiers = {
            'A': {},
            'E': [],
            'L': [],
            'Q': [],
            'T': [],
            'V': [],
            'S': [],
            'P': []
        }

        # Affect
        if tree.has("object_modifier_affect"):
            for object_modifier_affect in tree.get_all(
                    "object_modifier_affect"):
                affect = parser.Tree(object_modifier_affect).get_values()
                affect_type = types.MODIFIER_AFFECT_TYPES[affect[0] - 1]
                affect_value = affect[1]

                self.modifiers['A'][affect_type] = affect_value

        # Extra
        if tree.has("object_modifier_extra"):
            for object_modifier_extra in tree.get_all(
                    "object_modifier_extra"):
                extra = parser.Tree(object_modifier_extra).get_values()
                self.modifiers['E'].append({
                    "keywords": extra[0].split(" "),
                    "description": extra[1]
                })

        # Level
        if tree.has("object_modifier_level"):
            self.modifiers['L'].append(
                tree.get("object_modifier_level").get_value()
            )

        # Timer
        if tree.has("object_modifier_timer"):
            self.modifiers['T'].append(
                tree.get("object_modifier_timer").get_value()
            )

        # Quality (Cygnus Division)
        if tree.has("object_modifier_quality"):
            self.modifiers['Q'].append(
                tree.get("object_modifier_quality").get_value()
            )

        # Unknown (Cygnus Division)
        if tree.has("object_modifier_v_unknown"):
            self.modifiers['V'].append(
                tree.get("object_modifier_v_unknown").get_value()
            )

        # Spell (Cygnus Division)
        if tree.has("object_modifier_spell"):
            for object_modifier_spell in tree.get_all(
                    "object_modifier_spell"):
                spell = parser.Tree(object_modifier_spell).get_values()
                self.modifiers['S'].append({
                    "spell": spell[0],
                    "timer": spell[1],
                    "level": spell[2],
                    "auto": spell[3]
                })

        # Assemble (Cygnus Division)
        if tree.has("object_modifier_assemble"):
            for object_modifier_assemble in tree.get_all(
                    "object_modifier_assemble"):
                assemble = parser.Tree(
                    object_modifier_assemble
                ).get_values()
                self.modifiers['P'].append({
                    "source_vnum": assemble[0],
                    "target_vnum": assemble[1],
                    "chance": assemble[2],
                    "room_vnum": assemble[3],
                    "success": assemble[4],
                    "failure": assemble[5],
                    "time_limit": assemble[6:]
                })


class Room(Entity):
    def __init__(self, tree):
        super().__init__(tree)

        texts = tree.get("room_texts").get_values()
        (self.name,
         self.description) = texts[:2]
        if len(texts) > 2:
            self.night_description = texts[2]

        room_type = tree.get("room_type").get_values()
        (self.unused_flag,
         self.flags,
         self.type) = room_type[:3]
        if len(room_type) > 3:
            self.teleport_delay = room_type[3]
        if len(room_type) > 4:
            self.teleport_vnum = room_type[4]
        if len(room_type) > 5:
            self.tunnel = room_type[5]

        # Directions
        if tree.has("room_direction"):
            self.directions = {}
            for room_direction in tree.get_all("room_direction"):
                direction_values = parser.Tree(room_direction).get_values()
                dir_num = direction_values[0]
                direction = {
                    'description': direction_values[1],
                    'door_name': direction_values[2],
                    'exit_info': direction_values[3],
                    'key_vnum': direction_values[4],
                    'target_vnum': direction_values[5],
                }
                if len(direction_values) > 6:
                    direction['distance'] = direction_values[6]
                if len(direction_values) > 7:
                    direction['pull_type'] = direction_values[7]
                if len(direction_values) > 8:
                    direction['pull_strength'] = direction_values[8]
                self.directions[dir_num] = direction

        # Extra
        if tree.has("room_extra"):
            self.extra = []
            for room_extra in tree.get_all("room_extra"):
                examine = parser.Tree(room_extra).get_values()
                self.extra.append({
                    "keywords": examine[0].split(" "),
                    "description": examine[1]
                })

        # Map
        if tree.has("room_map"):
            self.map = {}
            for room_map in tree.get_all("room_map"):
                map_unused = parser.Tree(room_map).get_values()
                self.map[map_unused[0]] = {
                    "x": map_unused[1],
                    "y": map_unused[2],
                    "entry": map_unused[3]
                }


class Reset:
    class ObjectHide:
        def __init__(self, values, chance):
            if chance is not None:
                self.chance = chance

            (self.extra,
             self.object,
             self.status) = values[:3]
            if len(values) > 3:
                 self.argument = values[3]

    class ObjectTrap:
        def __init__(self, values, chance):
            if chance is not None:
                self.chance = chance

            (self.flags,
             self.type,
             self.charges,
             self.object) = values

    class Labyrinth:
        def __init__(self, values, chance):
            if chance is not None:
                self.chance = chance

            (self.extra,
             self.room,
             self.direction) = values

    class DoorStatus:
        def __init__(self, values, chance):
            if chance is not None:
                self.chance = chance

            (self.extra,
             self.room,
             self.direction,
             self.status) = values

    class MobLocation:
        def __init__(self, values, chance):
            if chance is not None:
                self.chance = chance

            (self.extra,
             self.mobile,
             self.limit,
             self.room) = values

    class MobInventory:
        def __init__(self, values, chance):
            if chance is not None:
                self.chance = chance

            (self.extra,
             self.object,
             self.limit) = values

    class MobEquipment:
        def __init__(self, values, chance):
            if chance is not None:
                self.chance = chance

            (self.extra,
             self.object,
             self.limit,
             self.location) = values

    class ObjectLocation:
        def __init__(self, values, chance):
            if chance is not None:
                self.chance = chance

            (self.extra,
             self.object,
             self.limit,
             self.room) = values

    class ObjectContainer:
        def __init__(self, values, chance):
            if chance is not None:
                self.chance = chance

            (self.nested,
             self.object,
             self.limit,
             self.container) = values

    def __init__(self):
        pass

    @staticmethod
    def factory(name, values, chance):
        if name == "reset_mob_location":
            return Reset.MobLocation(values, chance)
        elif name == "reset_object":
            return Reset.ObjectLocation(values, chance)
        elif name == "reset_container":
            return Reset.ObjectContainer(values, chance)
        elif name == "reset_mob_object":
            return Reset.MobInventory(values, chance)
        elif name == "reset_mob_equipment":
            return Reset.MobEquipment(values, chance)
        elif name == "reset_door":
            return Reset.DoorStatus(values, chance)
        elif name == "reset_labyrinth":
            return Reset.Labyrinth(values, chance)
        elif name == "reset_trap":
            return Reset.ObjectTrap(values, chance)
        elif name == "reset_hide":
            return Reset.ObjectHide(values, chance)
        raise Exception("Unhandled: %s" % name)


class Shop:
    def __init__(self, tree):
        shop = tree.get_values()
        self.vnum = shop[0]
        self.buys = shop[1:6]
        (self.buy_percent,
         self.sell_percent,
         self.opens,
         self.closes) = shop[6:10]


class CShop:
    def __init__(self, tree):
        cshop = tree.get_values()
        (self.vnum,
         self.clan) = cshop


class Repair:
    def __init__(self, tree):
        repair = tree.get_values()
        self.vnum = repair[0]
        self.repairs = repair[1:4]
        (self.profit,
         self.type,
         self.opens,
         self.closes) = repair[4:8]


class Help:
    def __init__(self, tree):
        help = tree.get_values()
        (self.level,
         self.keywords,
         self.body) = help


class Special:
    class Mobile:
        def __init__(self, values):
            (self.mobile,
             self.type) = values

    def __init__(self):
        pass

    @staticmethod
    def factory(name, values):
        if name == "special_mobile":
            return Special.Mobile(values)
        raise Exception("Unhandled: %s" % name)


class Distance:
    def __init__(self, tree):
        distance = tree.get_values()
        (self.filename,
         self.distance) = distance


class Area:
    def has_section(self, name):
       return hasattr(self, name.lower())

    def __init__(self, tree):
        is_chance = True
        if tree.has("area"):
            self.area = tree.get("area").get_value()

        if tree.has("author"):
            self.author = tree.get("author").get_value()

        if tree.has("ranges"):
            ranges = tree.get("ranges").get_values()
            self.ranges = (ranges[:2], ranges[2:])

        if tree.has("resetmsg"):
            self.resetmsg = tree.get("resetmsg").get_value()

        if tree.has("flags"):
            self.flags = tree.get("flags").get_values()

        if tree.has("version"):
            self.version = tree.get("version").get_values()
            if "VER_NOEXT_RESETS" in self.version:
                is_chance = False

        if tree.has("economy"):
            self.economy = tree.get("economy").get_values()

        if tree.has("neighbourhood"):
            self.neighbourhood = tree.get("neighbourhood").get_values()

        if tree.has("recall"):
            self.recall = tree.get("recall").get_value()

        if tree.has("temple"):
            self.temple = tree.get("temple").get_value()

        if tree.has("multiplierqp"):
            self.multiplierqp = tree.get("multiplierqp").get_values()
            if len(self.multiplierqp) == 1:
                self.multiplierqp = [self.multiplierqp[0]] * 9

        if tree.has("mobiles"):
            self.mobiles = {}
            for mobile_tree in tree.get("mobiles").get_all("mobile"):
                mobile = Mobile(parser.Tree(mobile_tree))
                self.mobiles[mobile.vnum] = mobile

        if tree.has("objects"):
            self.objects = {}
            for object_tree in tree.get("objects").get_all("object"):
                item = Object(parser.Tree(object_tree))
                self.objects[item.vnum] = item

        if tree.has("rooms"):
            self.rooms = {}
            for room_tree in tree.get("rooms").get_all("room"):
                item = Room(parser.Tree(room_tree))
                self.rooms[item.vnum] = item

        if tree.has("resets"):
            self.resets = []
            for reset_tree in tree.get("resets").get_all("reset"):
                reset_chance = None
                if is_chance:
                    reset_chance = int(reset_tree.children[1])
                reset = parser.Tree(list(reset_tree.children)[0])
                reset_name = reset.name
                reset_values = reset.get_values()
                self.resets.append(
                    Reset.factory(
                        reset_name,
                        reset_values,
                        reset_chance
                    )
                )

        if tree.has("specials"):
            self.specials = []
            for special_tree in tree.get("specials").get_all("special"):
                special = parser.Tree(list(special_tree.children)[0])
                special_name = special.name
                special_values = special.get_values()
                self.specials.append(
                    Special.factory(special_name, special_values)
                )

        if tree.has("shops"):
            self.shops = {}
            for shop_tree in tree.get("shops").get_all("shop"):
                shop = Shop(parser.Tree(shop_tree))
                self.shops[shop.vnum] = shop

        if tree.has("distances"):
            self.distances = {}
            for distance_tree in tree.get("distances").get_all("distance"):
                distance = Distance(parser.Tree(distance_tree))
                self.distances[distance.filename] = distance

        if tree.has("repairs"):
            self.repairs = {}
            for repair_tree in tree.get("repairs").get_all("repair"):
                repair = Repair(parser.Tree(repair_tree))
                self.repairs[repair.vnum] = repair

        if tree.has("cshops"):
            self.cshops = {}
            for cshop_tree in tree.get("cshops").get_all("cshop"):
                cshop = CShop(parser.Tree(cshop_tree))
                self.cshops[cshop.vnum] = cshop

        if tree.has("helps"):
            self.helps = []
            for help_tree in tree.get("helps").get_all("help"):
                help = Help(parser.Tree(help_tree))
                self.helps.append(help)
