import lark

AREA_GRAMMAR_FILE = "cygnus/grammar.area"


class Tree:
    def __init__(self, parsed_tree):
        self.parsed_tree = parsed_tree
        self.name = self.parsed_tree.data

    @staticmethod
    def from_string(input, grammar):
        lark_parser = lark.Lark(grammar)
        parsed = lark_parser.parse(input)
        return Tree(parsed)

    @staticmethod
    def cast(token):
        if token.type == "INTEGER":
            return int(token)
        if token.type == "VECTOR":
            return [int(x) for x in str(token).strip().split("&")]
        elif token.type == "VNUM":
            return int(token)
        elif token.type == "TEXT":
            return str(token).strip().rstrip("~").replace("\r\n", "\n")
        return str(token).strip()

    def has(self, name):
        parsed_tree = list(self.parsed_tree.find_data(name))
        return len(parsed_tree) > 0

    def get(self, name):
        parsed_tree = list(self.parsed_tree.find_data(name))
        if len(parsed_tree) == 0:
            raise Exception(
                "Element %s of %s not found" % (name, self.name)
            )
        return Tree(parsed_tree[0])

    def get_all(self, name):
        parsed_tree = list(self.parsed_tree.find_data(name))
        return parsed_tree

    def children(self):
        return self.parsed_tree.children

    def get_tokens(self):
        return list(
            filter(lambda x: x.type != "NL", self.parsed_tree.children)
        )

    def get_value(self):
        return self.get_values()[0]

    def get_values(self):
        return [Tree.cast(item) for item in self.get_tokens()]
