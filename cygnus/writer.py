from cygnus.types import (
    ITEM_TYPES,
    MODIFIER_AFFECT_TYPES,
    MODIFIER_TYPES,
    PROG_TYPES,
    SECTION_TYPES,
    SECTOR_TYPES,
    SPECIAL_TYPES,
    VERSION_TYPES
)

from cygnus import Reset, Special, Shop


def stringify(input):
    return [str(item) for item in input]


def separate(values, separator=" "):
    return separator.join(stringify(values))


class Writer:
    def __init__(self, filename, debug_mode, verbose_mode):
        self.filename = filename
        self.output = open(filename, "w")
        self.debug_mode = debug_mode
        self.verbose_mode = verbose_mode
        if self.debug_mode:
            self.output_debug = open("%s.debug" % filename, "w")

    def debug(self, entity, message):
        vnum = False
        if hasattr(entity, "vnum"):
            vnum = entity.vnum

        debug_message = "%s%s: %s" % (
            type(entity).__name__,
            (" [vnum: %d]" % vnum) if vnum else "",
            message
        )

        if self.debug_mode:
            self.output_debug.write(debug_message + "\n")
        if self.verbose_mode:
            print(debug_message)

    def write_multiline_section(self, name, lines=[], terminator=None):
        self.output.write("#%s\n" % str(name))
        if len(lines):
            self.output.write("\n".join(lines))
        if terminator:
            if len(lines):
                self.output.write("\n")
            self.output.write("%s" % terminator)

    def write_value_section(self, name, value):
        self.output.write("#%s %s" % (name, value))

    def write_text_section(self, name, text):
        self.write_value_section(name, "%s~" % text)

    def write_separator(self):
        self.output.write("\n\n")

    def write_progs(self):
        pass


class CygnusRebornWriter(Writer):
    def write(self, area):
        for section in SECTION_TYPES:
            if area.has_section(section):
                section_writer_method = "write_%s" % section.lower()
                section_writer = getattr(self, section_writer_method)
                section_writer(area)
                self.write_separator()
        for unsupported_section in ("DISTANCES", "CSHOPS"):
            if area.has_section(unsupported_section):
                self.debug(area, "%s section is not supported"
                           % unsupported_section)
        self.write_area_terminator()

    def write_area_terminator(self):
        self.write_multiline_section("$")

    def write_area(self, area):
        self.write_text_section("AREA", area.area)

    def write_author(self, area):
        self.write_text_section("AUTHOR", area.author)

    def write_ranges(self, area):
        (soft_range, hard_range) = area.ranges
        self.write_multiline_section(
            "RANGES",
            ["%d %d %d %d" % (soft_range[0], soft_range[1],
                              hard_range[0], hard_range[1])],
            "$")

    def write_resetmsg(self, area):
        self.write_text_section("RESETMSG", area.resetmsg)

    def write_flags(self, area):
        self.write_multiline_section("FLAGS", [
            separate(area.flags)
        ])

    def write_version(self, area):
        versions = []
        for version in area.version:
            if version not in VERSION_TYPES:
                self.debug(
                    area,
                    "%s version flag is not supported." % version
                )
            else:
                versions.append(version)
        self.write_multiline_section(
            "VERSION",
            versions,
            "#0"
        )

    def write_economy(self, area):
        self.write_value_section(
            "ECONOMY",
            separate(area.economy)
        )

    def write_multiplierqp(self, area):
        self.write_value_section(
            "MULTIPLIERQP",
            separate(area.multiplierqp)
        )

    def write_neighbourhood(self, area):
        self.write_multiline_section(
            "NEIGHBOURHOOD",
            area.neighbourhood,
            "$"
        )

    def write_shops(self, area):
        shops = [self.get_shop_string(shop) for vnum, shop in
                 area.shops.items()]
        self.write_multiline_section(
            "SHOPS",
            shops,
            "0"
        )

    def write_repairs(self, area):
        repairs = [self.get_repair_string(repair) for vnum, repair in
                   area.repairs.items()]
        self.write_multiline_section(
            "REPAIRS",
            repairs,
            "0"
        )

    def write_cshops(self, area):
        cshops = ["%d %s" % (vnum, cshop.clan)
                  for vnum, cshop
                  in area.cshops.items()]
        self.write_multiline_section(
            "CSHOPS",
            cshops,
            "0"
        )

    def write_distances(self, area):
        distances = ["%s %s" % (filename, distance.distance)
                     for filename, distance
                     in area.distances.items()]
        self.write_multiline_section(
            "DISTANCES",
            distances,
            "$"
        )

    def write_specials(self, area):
        specials = []
        for special in area.specials:
            special_string = self.get_special_string(special, area)
            if special_string:
                specials.append(special_string)
        self.write_multiline_section(
            "SPECIALS",
            specials,
            "S"
        )

    def write_mobiles(self, area):
        mobiles = [self.get_mobile_string(mobile, area)
                   for vnum, mobile
                   in area.mobiles.items()]
        self.write_multiline_section(
            "MOBILES",
            mobiles,
            "#0"
        )

    def write_objects(self, area):
        objects = [self.get_object_string(item, area)
                   for vnum, item
                   in area.objects.items()]
        self.write_multiline_section(
            "OBJECTS",
            objects,
            "#0"
        )

    def write_rooms(self, area):
        rooms = [self.get_room_string(room, area)
                 for vnum, room
                 in area.rooms.items()]
        self.write_multiline_section(
            "ROOMS",
            rooms,
            "#0"
        )

    def write_resets(self, area):
        resets = [self.get_reset_string(reset, area)
                  for reset
                  in area.resets]
        self.write_multiline_section(
            "RESETS",
            resets,
            "S"
        )

    def write_helps(self, area):
        helps = [self.get_help_string(help, area)
                 for help
                 in area.helps]
        self.write_multiline_section(
            "HELPS",
            helps,
            "0 $~"
        )

    def write_temple(self, area):
        self.write_value_section(
            "TEMPLE",
            area.temple
        )

    def write_recall(self, area):
        self.write_value_section(
            "RECALL",
            area.recall
        )

    # Entity helpers
    def get_help_string(self, help, area):
        return "\n".join([
            "%d %s~" % (help.level, help.keywords),
            "%s~" % help.body
        ])

    def get_reset_string(self, reset, area):
        line = ""
        if isinstance(reset, Reset.MobLocation):
            line += "%s %d %d %d %d" % (
                "M",
                reset.extra,
                reset.mobile,
                reset.limit,
                reset.room
            )
        elif isinstance(reset, Reset.ObjectLocation):
            line += "%s %d %d %d %d" % (
                "O",
                reset.extra,
                reset.object,
                reset.limit,
                reset.room
            )
        elif isinstance(reset, Reset.ObjectContainer):
            line += "%s %d %d %d %d" % (
                "G",
                reset.nested,
                reset.object,
                reset.limit,
                reset.container
            )
        elif isinstance(reset, Reset.MobInventory):
            line += "%s %d %d %d" % (
                "G",
                reset.extra,
                reset.object,
                reset.limit
            )
        elif isinstance(reset, Reset.MobEquipment):
            line += "%s %d %d %d %d" % (
                "E",
                reset.extra,
                reset.object,
                reset.limit,
                reset.location
            )
        elif isinstance(reset, Reset.DoorStatus):
            line += "%s %d %d %d %d" % (
                "D",
                reset.extra,
                reset.room,
                reset.direction,
                reset.status
            )
        elif isinstance(reset, Reset.Labyrinth):
            line += "%s %d %d %d" % (
                "R",
                reset.extra,
                reset.room,
                reset.direction
            )
        elif isinstance(reset, Reset.ObjectTrap):
            line += "%s %d %d %d %d" % (
                "T",
                reset.flags,
                reset.type,
                reset.charges,
                reset.object
            )
        elif isinstance(reset, Reset.ObjectHide):
            line += "%s %d %d %d" % (
                "H",
                reset.extra,
                reset.object,
                reset.status
            )
            if hasattr(reset, "argument"):
                line += " %d" % reset.argument
            else:
                self.debug(reset, "Wrong number of arguments. "
                                  "Filling gap with value -1.")
                line += " -1"

        if hasattr(reset, "chance"):
            line += " %d" % reset.chance
        return line

    def get_room_string(self, room, area):
        lines = [
            "#%d" % room.vnum,
            "%s~" % room.name,
            "%s~" % room.description
        ]
        if "VER_NITE_DESCS" in area.version:
            lines.append("%s~" % room.night_description)
        if room.type >= len(SECTOR_TYPES) or room.type < 0:
            if room.type == 24:  # SECT_GARDEN
                self.debug(room, "Sector type SECT_GARDEN is not "
                                 "supported. Converting to SECT_FOREST.")
                room.type = SECTOR_TYPES.index("SECT_FOREST")
        line = "%d %s %d" % (
            room.unused_flag,
            separate(room.flags, "&"),
            room.type
        )
        if hasattr(room, "teleport_delay"):
            line += " %d" % room.teleport_delay
        if hasattr(room, "teleport_vnum"):
            line += " %d" % room.teleport_vnum
        if hasattr(room, "tunnel"):
            line += " %d" % room.tunnel
        lines.append(line)

        if hasattr(room, "directions") and len(room.directions):
            lines.append(
                self.get_room_directions_string(room.directions, room)
            )

        if hasattr(room, "extra") and len(room.extra):
            lines.append(
                self.get_room_extras_string(room.extra, room)
            )

        if hasattr(room, "map") and len(room.map):
            lines.append(
                self.get_room_maps_string(room.map, room)
            )
        if hasattr(room, "progs"):
            progs_string = self.get_progs_string(room.progs, room)
            if progs_string:
                lines += [progs_string]

        lines.append("S")
        return "\n".join(lines)

    def get_room_directions_string(self, directions, room):
        lines = []
        for dir, direction in directions.items():
            values = "%d %d %d" % (
                direction['exit_info'],
                direction['key_vnum'],
                direction['target_vnum']
            )

            if "distance" in direction:
                values += " %d" % direction["distance"]
            if "pull_type" in direction:
                values += " %d" % direction["pull_type"]
            if "pull_strength" in direction:
                values += " %d" % direction["pull_strength"]

            lines += [
                "D%d" % dir,
                "%s~" % direction['description'],
                "%s~" % direction['door_name'],
                values
            ]
        return "\n".join(lines)

    def get_room_extras_string(self, extras, room):
        lines = []
        for extra in extras:
            lines += [
                "E",
                "%s~" % " ".join(extra['keywords']),
                "%s~" % extra['description']
            ]
        return "\n".join(lines)

    def get_room_maps_string(self, maps, room):
        lines = []
        for vnum, map in maps.items():
            lines.append("M %d %d %d %s" % (
                vnum,
                map['x'],
                map['y'],
                map['entry']
            ))
        return "\n".join(lines)

    def get_mobile_string(self, mobile, area):
        lines = [
            "#%d" % mobile.vnum,
            "%s~" % " ".join(mobile.keywords),
            "%s~" % mobile.name,
            "%s~" % mobile.display,
            "%s~" % mobile.description
        ]
        if "VER_NOPRZYP" not in area.version:
            lines += ["%s~" % case for case in mobile.grammar]

        money = self.get_money_string(mobile.money, mobile)
        lines += [
            "%s %s %d %s" % (separate(mobile.act, "&"),
                             separate(mobile.affected_by, "&"),
                             mobile.alignment,
                             "C" if mobile.is_complex else "S"),
            "%d %d %d %s %s" % (mobile.level,
                                mobile.to_hit,
                                mobile.armor_class,
                                mobile.hit_dice,
                                mobile.damage_dice),
            "%d %d" % (money, mobile.exp),
            "%d %d %d" % (
                mobile.position,
                mobile.def_position,
                mobile.sex)
        ]
        if mobile.is_complex:
            if len(mobile.resistant) > 1:
                self.debug(
                    mobile,
                    "Resistance is expected to be integer flag. Dropping"
                    " bit value &%d" % mobile.resistant[1])
            if len(mobile.immune) > 1:
                self.debug(
                    mobile,
                    "Immunity is expected to be integer flag. Dropping"
                    " bit value &%d" % mobile.immune[1])
            if len(mobile.susceptible) > 1:
                self.debug(
                    mobile,
                    "Susceptibility is expected to be integer flag."
                    " Dropping bit value &%d" % mobile.susceptible[1])

            mobile.resistant = mobile.resistant[0]
            mobile.immune = mobile.immune[0]
            mobile.susceptible = mobile.susceptible[0]

            lines += [
                "%d %d %d %d %d %d %d" % tuple(mobile.stats),
                "%d %d %d %d %d" % tuple(mobile.saves),
                "%d %d %d %d %d %d %d" % (
                    mobile.race,
                    mobile.class_type,
                    mobile.height,
                    mobile.weight,
                    mobile.languages_known,
                    mobile.language,
                    mobile.num_attacks),
                "%d %d %s %d %d %d %d %d %d" % (
                    mobile.hit_roll,
                    mobile.damage_roll,
                    separate(mobile.body_parts, "&"),
                    mobile.resistant,
                    mobile.immune,
                    mobile.susceptible,
                    mobile.attacks,
                    mobile.defenses,
                    mobile.corpse_vnum)
            ]

        if hasattr(mobile, "progs"):
            progs_string = self.get_progs_string(mobile.progs, mobile)
            if progs_string:
                lines.append(progs_string)

        return "\n".join(lines)

    def get_object_string(self, item, area):
        # ITEM_WORN -> ITEM_ARMOR
        if item.type == ITEM_TYPES.index("ITEM_WORN"):
            if 'MOD_ARMOR' in item.modifiers['A']:
                item.modifiers['A']['MOD_ARMOR'] -= item.values[0]
            else:
                item.modifiers['A']['MOD_ARMOR'] = -item.values[0]
            self.debug(item, "Type ITEM_WORN is not supported."
                             " Converting to ITEM_ARMOR with AC"
                             " changed to %d." %
                       item.modifiers['A']['MOD_ARMOR'])
            item.type = ITEM_TYPES.index("ITEM_ARMOR")

        lines = [
            "#%d" % item.vnum,
            "%s~" % " ".join(item.keywords),
            "%s~" % item.name,
            "%s~" % item.display,
            "%s~" % item.description
        ]

        if "VER_NOPRZYP" not in area.version:
            lines += ["%s~" % case for case in item.grammar]

        money = self.get_money_string(item.cost, item)
        weight_cost = [
            item.weight,
            money,
            item.material_type
        ]
        if "VER_NOGENDER" not in area.version:
            weight_cost.append(item.sex)
        if "VER_SIZE" in area.version:
            weight_cost.append(item.size)

        line = "%d %s %d" % (
            item.type,
            separate(item.extra_flags, "&"),
            item.wear_flags
        )

        if hasattr(item, "layers"):
            line += " %d" % item.layers

        lines += [
            line,
            separate(item.values),
            separate(weight_cost)
        ]

        is_spell_type_item = item.type in (
            ITEM_TYPES.index("ITEM_PILL"),
            ITEM_TYPES.index("ITEM_POTION"),
            ITEM_TYPES.index("ITEM_SCROLL"),
            ITEM_TYPES.index("ITEM_STAFF"),
            ITEM_TYPES.index("ITEM_WAND"),
            ITEM_TYPES.index("ITEM_SALVE")
        ) and len(item.spell_names) > 0
        is_text_spell_version = "VER_NOTXTSPELL" not in area.version

        if is_spell_type_item and is_text_spell_version:
            spell_count = 3
            if item.type in (
                    ITEM_TYPES.index("ITEM_STAFF"),
                    ITEM_TYPES.index("ITEM_WAND"),
                    ITEM_TYPES.index("ITEM_SALVE")):
                spell_count = 2
            spell_names = item.spell_names + ['NONE'] * spell_count
            spell_names = ["'%s'" % spell_name
                           for spell_name
                           in spell_names[:spell_count]]
            lines.append(" ".join(spell_names))

        for modifier_type, modifiers in item.modifiers.items():
            if len(modifiers) > 0:
                if modifier_type not in MODIFIER_TYPES:
                    self.debug(item, "Unsupported modifier %s ignored."
                               % modifier_type)
                else:
                    lines.append(
                        self.get_object_modifiers_string(
                            modifiers,
                            modifier_type,
                            item))

        if hasattr(item, "progs"):
            progs_string = self.get_progs_string(item.progs, item)
            if progs_string:
                lines += [progs_string]

        return "\n".join(lines)

    def get_money_string(self, money, entity):
        original = money
        money = original
        if isinstance(original, list):
            (coin, denar, dukat) = original
            denar = (dukat * 100) + denar
            coin = (denar * 100) + coin
            money = coin * 100
            self.debug(entity, "Convert incompatible money"
                               " format (%s -> %d)"
                       % (separate(original), money))
        return money

    def get_object_modifiers_string(self, modifiers, modifier_type, item):
        lines = []

        if modifier_type == "A":
            for affect_type, value in modifiers.items():
                affect_id = MODIFIER_AFFECT_TYPES.index(
                    affect_type) + 1
                lines.append("A\n%d %d" % (affect_id, value))

        elif modifier_type == "E":
            for modifier in modifiers:
                lines.append(
                    "E\n%s~\n%s~" % (
                        " ".join(modifier['keywords']),
                        modifier["description"]))

        else:
            for modifier in modifiers:
                lines.append("%s\n%s" % (modifier_type, str(modifier)))

        return "\n".join(lines)

    def get_progs_string(self, progs, entity):
        lines = []
        if progs and len(progs) > 0:
            for prog in progs:
                if prog.type not in PROG_TYPES:
                    self.debug(entity, "Program %s is not supported"
                               % prog.type)
                else:
                    lines.append(self.get_prog_string(prog))
            lines.append("|")
            return "\n".join(lines)
        return None

    def get_prog_string(self, prog):
        return "> %s %s~\n%s~" % (
            prog.type,
            prog.arguments,
            prog.body
        )

    def get_special_string(self, special, area):
        if isinstance(special, Special.Mobile):
            if special.type in SPECIAL_TYPES:
                return self.get_mobile_special_string(special, area)
            else:
                self.debug(special, "%s type is not supported" %
                           special.type)
        return None

    def get_mobile_special_string(self, mobile_special, area):
        return "M  %d %s" % (mobile_special.mobile, mobile_special.type)

    def get_shop_string(self, shop):
        values = list(
            [shop.vnum]
            + shop.buys
            + [shop.buy_percent,
               shop.sell_percent,
               shop.opens,
               shop.closes]
        )
        return "%d   %d %d %d %d %d   %d %d   %d %d" % tuple(values)

    def get_repair_string(self, repair):
        values = list(
            [repair.vnum]
            + repair.repairs
            + [repair.profit,
               repair.type,
               repair.opens,
               repair.closes]
        )
        return "%d   %d %d %d   %d %d   %d %d" % tuple(values)
