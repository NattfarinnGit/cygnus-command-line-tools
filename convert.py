#!/usr/bin/env python3

import argparse
import glob
import os

from cygnus import writer, parser, Area


if __name__ == "__main__":
    command_line = argparse.ArgumentParser(
        description="Convert area from Cygnus Division to Cygnus Reborn "
                    "format.",
    )

    command_line.add_argument(
        "sources",
        type=str,
        nargs="+",
        help="input file path (may be glob type, somewhere/*.are)"
    )

    command_line.add_argument(
        "--target",
        type=str,
        nargs="?",
        default="./output",
        help="target directory (default: ./output, will be created if "
             "path does not exist yet)"
    )

    command_line.add_argument(
        "--no-debug-files",
        action="store_true",
        default=False,
        help="Do not write debug files"
    )

    command_line.add_argument(
        "--verbose",
        action="store_true",
        default=False,
        help="Print debug information"
    )

    command_line.add_argument(
        "--encoding",
        default="latin2",
        type=str,
        nargs="?",
        help="Input file(s) encoding (default: latin2)"
    )

    args = command_line.parse_args()

    input_sources = args.sources
    input_encoding = args.encoding
    output_directory = args.target
    no_debug_files = args.no_debug_files
    verbose = args.verbose

    input_files = []
    for input_source in input_sources:
        input_files += list(
            filter(
                lambda file: os.path.isfile(file),
                glob.glob(input_source)
            )
        )

    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    progress = 0
    total = len(input_files)

    with open(parser.AREA_GRAMMAR_FILE, "r") as grammar_file:
        lark_grammar = grammar_file.read()

        for area_path in input_files:
            filename = area_path.split("/")[-1]

            with open(area_path, "br") as area_file:
                area_content = area_file.read()
                area_content = area_content.decode(input_encoding)

                if area_content and lark_grammar:
                    area = Area(
                        parser.Tree.from_string(
                            area_content,
                            lark_grammar
                        )
                    )

                    area_writer = writer.CygnusRebornWriter(
                        "%s/%s" % (output_directory, filename),
                        not no_debug_files,
                        verbose
                    )

                    area_writer.write(area)
                    progress += 1
                    print("%d/%d (%.2f%%), %s done." % (
                        progress,
                        total,
                        (progress/total) * 100,
                        filename,
                    ))
